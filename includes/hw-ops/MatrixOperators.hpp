/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
 */

#pragma once

#include "MatrixAdd.hpp"
#include "MatrixElementWise.hpp"
#include "MatrixMap.hpp"
#include "MatrixMultiply.hpp"
#include "MatrixMultiplyAdd.hpp"
#include "MatrixMultiplyAddStrassen.hpp"
#include "MatrixMultiplyAddWinograd.hpp"
#include "MatrixMultiplyStrassen.hpp"
#include "MatrixMultiplyWinograd.hpp"

#include "MatrixReduce.hpp"
